package snhu.jukebox.playlist;

import snhu.student.playlists.*;

import java.util.ArrayList;
import java.util.List;

public class StudentList {

	public StudentList(){
	}

	public List<String> getStudentsNames() {
		ArrayList<String> studentNames = new ArrayList<String>();
		
		String StudentName1 = "TestStudent1Name";
		studentNames.add(StudentName1);
		
		String StudentName2 = "TestStudent2Name";
		studentNames.add(StudentName2);
		

		//Added myself "Korey Hardy" to the jukebox app
		String KoreyHardy = "KoreyHardy";
		studentNames.add(KoreyHardy);
		
		//Added myself "Donna Baijnauth" to the jukebox app
		String DonnaBaijnauth = "DonnaBaijnauth";
		studentNames.add(DonnaBaijnauth);
		
		//Added myself "Evan Wallesen" to the jukebox app
		String EvanWallesen = "Evan Wallesen";
		studentNames.add(EvanWallesen);
		
		//Added myself "Alan Davis" to the jukebox app
		String AlanDavis = "Alan Davis";
		studentNames.add(AlanDavis);

		//Module 5 Code Assignment
		//Add your name to create a new student profile
		//Use template below and put your name in the areas of 'StudentName'
		
		// Add name using template		
		String NathanBurnham = "NathanBurnham";
		studentNames.add(NathanBurnham);
		
		//Added name to jukebox app 2019-04-07
		String AmberNathan = "Amber Nathan";
		studentNames.add(AmberNathan);
		
		 //Added myself "Bourama Mangara" to the jukebox app
		String bouramaMangara = "BouramaMangara";
		studentNames.add(bouramaMangara);
		
		return studentNames;
	}

	public Student GetStudentProfile(String student){
		Student emptyStudent = null;
	
		switch(student) {
		   case "TestStudent1_Playlist":
			   TestStudent1_Playlist testStudent1Playlist = new TestStudent1_Playlist();
			   Student TestStudent1 = new Student("TestStudent1", testStudent1Playlist.StudentPlaylist());
			   return TestStudent1;
			   
		   case "TestStudent2_Playlist":
			   TestStudent2_Playlist testStudent2Playlist = new TestStudent2_Playlist();
			   Student TestStudent2 = new Student("TestStudent2", testStudent2Playlist.StudentPlaylist());
			   return TestStudent2;
			   
			   
		   //Module 6 Code Assignment - Add your own case statement for your profile. Use the above case statements as a template.
			   
		   case "KoreyHardy_Playlist":
			   KoreyHardy_Playlist koreyHardyPlaylist = new KoreyHardy_Playlist();
			   Student KoreyHardy = new Student("Korey Hardy", koreyHardyPlaylist.StudentPlaylist());
			   return KoreyHardy;
			   
		   case "Bourama_Playlist":
			   Bourama_Playlist bMangaraPlaylist = new Bourama_Playlist();
			   Student BouramaMangara = new Student("BouramaMangara", bMangaraPlaylist.StudentPlaylist());
			   return BouramaMangara;


		}
		return emptyStudent;
	}
}
