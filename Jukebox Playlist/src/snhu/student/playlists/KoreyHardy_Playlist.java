//importing packages
package snhu.student.playlists;
import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

//logic to implement playlist
public class KoreyHardy_Playlist {
    public LinkedList<PlayableSong> StudentPlaylist(){
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	
	//adding songs from Creed
	ArrayList<Song> creedTracks = new ArrayList<Song>();
    Creed creedBand = new Creed();
	creedTracks = creedBand.getCreedSongs();
	
	playlist.add(creedTracks.get(0)); //My Own Prison
	playlist.add(creedTracks.get(1)); //One
	
	//adding songs from Slipknot
    Slipknot slipknotBand = new Slipknot();
	ArrayList<Song> slipknotTracks = new ArrayList<Song>();
    slipknotTracks = slipknotBand.getSlipknotSongs();
	
	playlist.add(slipknotTracks.get(0)); //The Devil in I
	playlist.add(slipknotTracks.get(1)); //Duality
	playlist.add(slipknotTracks.get(2)); //Killpop
	
	//adding songs from Queen
    Queen queenBand = new Queen();
	ArrayList<Song> queenTracks = new ArrayList<Song>();
    queenTracks = queenBand.getQueenSongs();
	
	playlist.add(queenTracks.get(0)); //Bohemian Rhapsody
	
	//adding songs from PostMalone
    PostMalone postmaloneBand = new PostMalone();
	ArrayList<Song> postmaloneTracks = new ArrayList<Song>();
    postmaloneTracks = postmaloneBand.getPostMaloneSongs();
	
	playlist.add(postmaloneTracks.get(0)); //Psycho
	playlist.add(postmaloneTracks.get(1)); //Rockstar
	
    return playlist;
	}
}