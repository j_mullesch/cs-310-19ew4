package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class ABBA {
	
	ArrayList<Song> albumTracks;
	String albumTitles;
	
	public ABBA() {
	}
	
	public ArrayList<Song> getABBASongs() {
		
		albumTracks = new ArrayList<Song>();						//Instantiate the album so we can populate it below
		Song track1 = new Song("Super Trouper", "ABBA");			//Create a song
		Song track2 = new Song("Waterloo", "ABBA");					//Create another song
		Song track3 = new Song("Take A Chance On Me", "ABBA");		//Create another song
		this.albumTracks.add(track1);								//Add the first song to the song list for ABBA
		this.albumTracks.add(track2);								//Add the second song to the song list for ABBA
		this.albumTracks.add(track3);								//Add the third song to the song list for ABBA
		return albumTracks;											//Return the songs for ABBA in the form on an Array List
	}

}
