package music.artist;


import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class AntiFlag {
		
	ArrayList<Song> albumTracks;
	String albumTitle;
		
	public AntiFlag() {
	}
		
	public ArrayList<Song> getAntiFlagSongs() {
			
		albumTracks = new ArrayList<Song>();							// Instantiate album
		Song track1 = new Song("American Attraction", "AntiFlag");		// Create songs
		Song track2 = new Song("Racists", "AntiFlag");			
		Song track3 = new Song("Drink Drank Punk", "AntiFlag");
		this.albumTracks.add(track1);									// Add songs to song list
		this.albumTracks.add(track2);
		this.albumTracks.add(track3);
		return albumTracks;												// Return songs in form of array list
	}
}
