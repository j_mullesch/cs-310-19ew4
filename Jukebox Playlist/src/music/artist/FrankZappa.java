package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class FrankZappa {
	
	ArrayList<Song> albumTracks;
	String albumTitle;
	
	public FrankZappa() {
	}
	
	public ArrayList<Song> getFrankZappaSongs() {
		
		albumTracks = new ArrayList<Song>();							// Instantiate album
		Song track1 = new Song("Camarillo Brillo", "Frank Zappa");		// Create songs
		Song track2 = new Song("Muffin Man", "Frank Zappa");			
		this.albumTracks.add(track1);									// Add songs to song list
		this.albumTracks.add(track2);
		return albumTracks;												// Return songs in form of array list
	}
}
