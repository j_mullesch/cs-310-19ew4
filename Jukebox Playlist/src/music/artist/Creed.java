package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Creed {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Creed() {
    }
    
    public ArrayList<Song> getCreedSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("My Own Prison", "Creed");                     //Create a song
         Song track2 = new Song("One", "Creed");                                //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Creed
         this.albumTracks.add(track2);                                          //Add the second song to song list for Creed 
         return albumTracks;                                                    //Return the songs for Creed in the form of an ArrayList
    }
}